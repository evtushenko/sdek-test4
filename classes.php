<?php

interface IDrawable
{
    public function draw();
}

abstract class Shape implements IDrawable
{
    abstract public function getPerimeter();
    abstract public function getArea();
}

class Triangle extends Shape
{
    private $ab;
    private $bc;
    private $ca;

    public function __construct($ab, $bc, $ca)
    {
        if ($ab < 0) {
            throw new InvalidArgumentException('ab');
        }
        if ($bc < 0) {
            throw new InvalidArgumentException('bc');
        }
        if ($ca < 0) {
            throw new InvalidArgumentException('ca');
        }

        if (($ab >= $bc + $ca) || ($bc >= $ca + $ab) || ($ca >= $ab + $bc)) {
            throw new InvalidArgumentException('inequality');
        }

        $this->ab = $ab;
        $this->bc = $bc;
        $this->ca = $ca;
    }

    public function getAb()
    {
        return $this->ab;
    }

    public function getBc()
    {
        return $this->bc;
    }

    public function getCa()
    {
        return $this->ca;
    }

    public function draw()
    {
        return 'triangle';
    }

    public function getPerimeter()
    {
        return $this->getAb() + $this->getBc() + $this->getCa();
    }

    public function getArea()
    {
        $p = $this->getPerimeter() / 2;

        return sqrt($p * ($p - $this->getAb()) * ($p - $this->getBc()) * ($p - $this->getCa()));
    }
}

class IsoscelesTriangle extends Triangle
{
    public function __construct($ab, $ca)
    {
        parent::__construct($ab, $ab, $ca);
    }

    public function draw()
    {
        return 'isosceles triangle';
    }
}

class EquilateralTriangle extends IsoscelesTriangle
{
    public function __construct($ab)
    {
        parent::__construct($ab, $ab);
    }

    public function draw()
    {
        return 'equilateral triangle';
    }
}

class Rectangle extends Shape
{
    private $width;
    private $height;

    public function __construct($width, $height)
    {
        if ($width < 0) {
            throw new InvalidArgumentException('width');
        }
        if ($height < 0) {
            throw new InvalidArgumentException('height');
        }

        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function draw()
    {
        return 'rectangle';
    }

    public function getPerimeter()
    {
        return 2 * ($this->getWidth() + $this->getHeight());
    }

    public function getArea()
    {
        return $this->getWidth() * $this->getHeight();
    }
}

class Square extends Rectangle
{
    public function __construct($size)
    {
        parent::__construct($size, $size);
    }

    public function draw()
    {
        return 'square';
    }
}
