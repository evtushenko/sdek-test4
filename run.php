<?php
require_once __DIR__ . '/classes.php';

/** @var Shape[] $shapes */
$shapes = [
    new Triangle(2, 3, 4),
    new IsoscelesTriangle(2, 3),
    new EquilateralTriangle(4),
    new Rectangle(1, 2),
    new Square(3),
];

foreach ($shapes as $shape) {
    printf(
        '%s: perimeter = %.2f, area = %.2f' . PHP_EOL,
        $shape->draw(),
        $shape->getPerimeter(),
        $shape->getArea()
    );
}